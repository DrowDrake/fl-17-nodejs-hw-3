const Joi = require('joi');

const getLoadsSchema = Joi.object({
  status: Joi.string()
      .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED')
      .uppercase(),
  limit: Joi.number().integer().min(0).max(50),
  offset: Joi.number().integer().min(0),
});

module.exports = getLoadsSchema;
