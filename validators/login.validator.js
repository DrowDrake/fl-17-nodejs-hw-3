const Joi = require('joi');

const loginSchema = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().alphanum().min(3).max(30).required(),
});

module.exports = loginSchema;
