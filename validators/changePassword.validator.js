const Joi = require('joi');

const changePasswordSchema = Joi.object({
  oldPassword: Joi.string().min(3).max(30).required(),
  newPassword: Joi.string().min(3).max(30).required(),
});

module.exports = changePasswordSchema;
