const register = require('./register.validator');
const login = require('./login.validator');
const forgotPassword = require('./forgotPassword.validator');

const changePassword = require('./changePassword.validator');

const createTruck = require('./createTruck.validator');
const updateTruck = require('./updateTruck.vakidator');

const createLoad = require('./createLoad.validator');
const updateLoad = require('./updateLoad.validator');
const getLoads = require('./getLoads.validator');

module.exports = {
  register,
  login,
  forgotPassword,
  changePassword,
  createTruck,
  updateTruck,
  createLoad,
  updateLoad,
  getLoads,
};
