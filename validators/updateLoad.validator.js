const Joi = require('joi');

const updateLoadSchema = Joi.object({
  name: Joi.string().required(),
  payload: Joi.number().integer().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: {
    width: Joi.number().integer().required(),
    length: Joi.number().integer().required(),
    height: Joi.number().integer().required(),
  },
});

module.exports = updateLoadSchema;
