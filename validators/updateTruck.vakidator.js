const Joi = require('joi');

const updateTruckSchema = Joi.object({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .uppercase()
      .required(),
});

module.exports = updateTruckSchema;
