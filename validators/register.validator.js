const Joi = require('joi');

const registerSchema = Joi.object({
  email: Joi.string().required().email(),
  password: Joi.string().alphanum().min(3).max(30).required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').uppercase().required(),
});

module.exports = registerSchema;
