const Joi = require('joi');

const forgotPasswordSchema = Joi.object({
  email: Joi.string().required().email(),
});

module.exports = forgotPasswordSchema;
