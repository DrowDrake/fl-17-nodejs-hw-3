const bcrypt = require('bcryptjs');
const User = require('../models/user.model');
const trucks = require('../models/truck.model');

module.exports = class UserController {
  /**
   * @param {*} req
   * @param {*} res
   */
  getById(req, res) {
    User.findOne(
        {_id: req.user.data.id},
        '-password -__v',
        function(err, item) {
          res.json({user: item ?? {}});
        },
    );
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async changePassword(req, res) {
    const user = await User.findOne({_id: req.user.data.id}, 'password');
    const match = await bcrypt.compare(req.body.oldPassword, user.password);
    const truck = await trucks.findOne({
      assigned_to: req.user.data.id,
      status: 'OL',
    });
    if (!truck && match) {
      const salt = bcrypt.genSaltSync(10);
      await User.findOneAndUpdate(
          {_id: req.user.data.id},
          {password: bcrypt.hashSync(req.body.newPassword, salt)},
          {
            returnOriginal: true,
          },
      );
      res.json({message: 'Success'});
    } else {
      res.status(400).json({message: 'Bad request'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async deleteUser(req, res) {
    const truck = await trucks.findOne({
      assigned_to: req.user.data.id,
    });
    if (!truck || truck.status == 'IS') {
      await User.deleteOne({_id: req.user.data.id})
          .then(function() {
            res.json({message: 'Profile deleted successfully'});
          })
          .catch(function(err) {
            res.status(500).json({message: err.message});
          });
    } else {
      res.status(400).json({message: `User can't be changed right now`});
    }
  }
};
