const trucks = require('../models/truck.model');
const users = require('../models/user.model');

module.exports = class TruckController {
  /**
   * @param {*} req
   * @param {*} res
   */
  async addTruck(req, res) {
    const userWithRole = await users.findOne({
      _id: req.user.data.id,
      role: 'DRIVER',
    });

    if (userWithRole) {
      await trucks.create({
        created_by: req.user.data.id,
        type: req.body.type,
        status: 'IS',
      });
      res.status(200).json({message: 'Truck created successfully'});
    } else {
      res.status(400).json({message: 'User role is not DRIVER'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async getAllTrucks(req, res) {
    const usersData = await users.findOne({
      _id: req.user.data.id,
      role: 'DRIVER',
    });
    if (usersData !== null) {
      const trucksData = await trucks.find(
          {created_by: req.user.data.id},
          '-__v',
      );
      res.status(200).json({trucks: trucksData ?? []});
    } else {
      res.status(200).json({trucks: []});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async getTruckById(req, res) {
    const truck = await trucks.findOne(
        {_id: req.params.id, created_by: req.user.data.id},
        '-__v',
    );
    const userWithRole = await users.findOne({
      _id: req.user.data.id,
      role: 'DRIVER',
    });
    if (userWithRole) {
      res.status(200).json({truck: truck ?? {}});
    } else {
      res.status(200).json({truck: {}});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async updateUserTruck(req, res) {
    const truck = await trucks.findOne(
        {
          _id: req.params.id,
          created_by: req.user.data.id,
          status: 'IS',
          assigned_to: {$ne: req.user.data.id},
        },
        '-__v',
    );

    if (!truck) {
      return res.status(400).json({message: 'No truck available'});
    }

    const userWithRole = await users.findOne({
      _id: req.user.data.id,
      role: 'DRIVER',
    });

    if (userWithRole) {
      truck.type = req.body.type;
      truck.save();
      res.status(200).json({message: 'Truck details changed successfully'});
    } else {
      res.status(400).json({message: 'User role is not DRIVER'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async deleteTruck(req, res) {
    const truck = await trucks.findOne(
        {
          _id: req.params.id,
          created_by: req.user.data.id,
          status: 'IS',
          assigned_to: {$ne: req.user.data.id},
        },
        '-__v',
    );

    if (!truck) {
      return res.status(400).json({message: 'No truck available'});
    }

    const userWithRole = await users.findOne({
      _id: req.user.data.id,
      role: 'DRIVER',
    });

    if (userWithRole) {
      await trucks.findByIdAndRemove(req.params.id);
      res.status(200).json({message: 'Truck deleted successfully'});
    } else {
      res.status(400).json({message: `Such user dosent't exists`});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async assignTruck(req, res) {
    const assignedTrucks = await trucks.findOne(
        {assigned_to: req.user.data.id},
        '-__v',
    );
    if (assignedTrucks) {
      return res.status(400).json({message: 'Truck allready assigned'});
    }
    const userWithRole = await users.findOne({
      _id: req.user.data.id,
      role: 'DRIVER',
    });
    const truckWithAssign = await trucks.findOne({
      _id: req.params.id,
    });
    const truck = await trucks.findOne({_id: req.params.id}, '-__v');

    if (userWithRole && truckWithAssign) {
      await trucks.updateOne(truck, {
        $set: {assigned_to: req.user.data.id},
      });
      res.status(200).json({message: 'Truck assigned successfully'});
    } else {
      res.status(400).json({message: `Truck wasn't assign`});
    }
  }
};
