const User = require('../models/user.model');
const bcrypt = require('bcryptjs');
const jwt = require('../jwt');

module.exports = class Auth {
  /**
   * @param {*} req
   * @param {*} res
   */
  registerUser(req, res) {
    const value = req.body;
    User.findOne({email: value.email}).then((user) => {
      if (user) {
        res.status(500).json({message: 'user olready exists'});
      } else {
        const newUser = new User({
          email: value.email,
          password: value.password,
          role: value.role,
        });
        bcrypt.genSalt(10, (err, salt) =>
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save().then(() => {
              res.status(200).json({
                message: 'Profile created successfully',
              });
            });
          }),
        );
      }
    });
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async loginUser(req, res) {
    const {email, password} = req.body;

    if (!email || !password) {
      res.status(400).json({message: 'All input is required'});
    }

    const user = await User.findOne({email: email});

    if (user && (await bcrypt.compare(password, user.password))) {
      const parsedUser = {id: user._id, role: user.role};
      const token = jwt.generateToken(parsedUser);
      res.status(200).json({
        jwt_token: token,
        message: 'Success',
      });
    } else {
      res.status(400).json({message: 'Invalid Credentials'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async forgotPassword(req, res) {
    const user = await User.findOne({email: req.body.email});
    if (user) {
      res
          .status(200)
          .json({message: 'New password sent to your email address'});
    } else {
      res.status(400).json({message: `Such user dosen't exists`});
    }
  }
};
