const loads = require('../models/load.model');
const trucks = require('../models/truck.model');
const states = require('../states/states');
const users = require('../models/user.model');

module.exports = class LoadController {
  /**
   * @param {*} req
   * @param {*} res
   */
  async addUserLoad(req, res) {
    if (req.user.data.role == 'SHIPPER') {
      await loads.create({
        created_by: req.user.data.id,
        name: req.body.name,
        payload: req.body.payload,
        pickup_address: req.body.pickup_address,
        delivery_address: req.body.delivery_address,
        status: 'NEW',
        dimensions: {
          width: req.body.dimensions.width,
          length: req.body.dimensions.length,
          height: req.body.dimensions.height,
        },
      });
      res.status(200).json({message: 'Load created successfully'});
    } else {
      res.status(400).json({message: 'You need to be a shipper'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async getUserLoads(req, res) {
    const limit = +req.query.limit || 10;
    const offset = +req.query.offset || 0;
    const statusData = req.query.status;
    const searchParams = {};

    if (statusData) {
      searchParams.status = statusData.toUpperCase();
    }

    if (req.user.data.role == 'SHIPPER') {
      const shipperLoads = await loads
          .find(searchParams)
          .skip(offset)
          .limit(limit);
      res.status(200).json({loads: shipperLoads ?? []});
    } else {
      const driverLoads = await loads.find({
        status: searchParams.status ?? {$in: ['ASSIGNED', 'SHIPPED']},
        assigned_to: req.user.data.id,
      });
      res.status(200).json({loads: driverLoads ?? []});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async getActiveLoad(req, res) {
    if (req.user.data.role == 'DRIVER') {
      const assignedLoads = await loads.findOne(
          {assigned_to: req.user.data.id, status: 'ASSIGNED'},
          '-__v',
      );
      res.status(200).json({load: assignedLoads ?? {}});
    } else {
      res.status(200).json({message: 'You need to be a driver'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async iterateToNextLoad(req, res) {
    if (req.user.data.role == 'DRIVER') {
      const assignedLoads = await loads.findOne(
          {assigned_to: req.user.data.id, status: 'ASSIGNED'},
          '-__v',
      );
      if (!assignedLoads) {
        return res.status(400).json({message: 'no such state'});
      }
      const loadParamsIndex = states.LOAD_PARAMS.indexOf(assignedLoads.state);
      if (loadParamsIndex < 3) {
        assignedLoads.state = states.LOAD_PARAMS[loadParamsIndex + 1];
        if (assignedLoads.state == states.LOAD_PARAMS[3]) {
          assignedLoads.status = 'SHIPPED';
          const user = await users.findOne({_id: assignedLoads.assigned_to});
          const truck = await trucks.findOne({assigned_to: user._id});
          truck.status = 'IS';
          truck.save();
        }
        assignedLoads.logs.push({
          message: `Load state changed to ${assignedLoads.state}`,
          time: Date.now(),
        });
        assignedLoads.save();
        res
            .status(200)
            .json({message: `Load state changed to ${assignedLoads.state}`});
      } else {
        res.status(400).json({message: 'no such state'});
      }
    } else {
      res.status(400).json({message: 'You need to be a driver'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async getUserLoadById(req, res) {
    const load = await loads.findOne({_id: req.params.id}, '-__v');
    res.status(200).json({load: load ?? {}});
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async updateUserLoad(req, res) {
    if (req.user.data.role == 'SHIPPER') {
      const load = await loads.findOne(
          {_id: req.params.id, status: 'NEW'},
          '-__v',
      );
      if (load) {
        await loads.updateOne(load, {
          $set: {
            name: req.body.name,
            payload: req.body.payload,
            pickup_address: req.body.pickup_address,
            delivery_address: req.body.delivery_address,
            dimensions: {
              width: req.body.dimensions.width,
              length: req.body.dimensions.length,
              height: req.body.dimensions.height,
            },
          },
        });
        res.status(200).json({message: 'Load details changed successfully'});
      } else {
        res
            .status(400)
            .json({message: `Such load dosent't exists or can't be changed`});
      }
    } else {
      res.status(400).json({message: 'You need to be a shipper'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async deleteUserLoad(req, res) {
    if (req.user.data.role == 'SHIPPER') {
      const deletedLoad = await loads.deleteOne({
        _id: req.params.id,
        status: 'NEW',
      });
      if (deletedLoad.deletedCount == 1) {
        res.status(200).json({message: 'Load deleted successfully'});
      } else {
        res
            .status(400)
            .json({message: `Such load dosent't exists or can't be changed`});
      }
    } else {
      res.status(400).json({message: 'You need to be a shipper'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async addUserLoadById(req, res) {
    if (req.user.data.role == 'SHIPPER') {
      const loadId = await loads.findOne(
          {_id: req.params.id, status: 'NEW'},
          '-__v',
      );
      if (!loadId) {
        return res.status(400).json({message: 'no loads detected'});
      }
      const trucksList = this.getTruckSize(loadId.dimensions, loadId.payload);
      const trucksBD = await trucks.findOne({
        type: {$in: trucksList},
        assigned_to: {$ne: null},
      });
      await loads.updateOne(loadId, {
        $set: {
          status: 'POSTED',
        },
      });
      if (trucksBD) {
        loadId.assigned_to = trucksBD.assigned_to;
        loadId.status = 'ASSIGNED';
        loadId.state = 'En route to Pick Up';
        loadId.logs.push({
          message:
            `Load assigned to driver with id ${trucksBD.assigned_to}` +
            `and load state changed to 'En route to Pick Up`,
          time: Date.now(),
        });
        loadId.save();
        await trucks.updateOne(trucksBD, {$set: {status: 'OL'}});
        res
            .status(200)
            .json({message: 'Load posted successfully', driver_found: true});
      } else {
        loadId.status = 'NEW';
        loadId.state = null;
        loadId.logs.push({
          message: `No approptiate car found`,
          time: Date.now(),
        });
        res.status(400).json({message: 'truck not found'});
      }
    } else {
      res.status(400).json({message: 'You need to be a shipper'});
    }
  }
  /**
   * @param {*} req
   * @param {*} res
   */
  async getShippingInfo(req, res) {
    if (req.user.data.role == 'SHIPPER') {
      const load = await loads.findOne({_id: req.params.id}, '-__v');
      const user = await users.findOne({_id: load.assigned_to});
      const truck = await trucks.findOne({assigned_to: user._id}, '-__v');
      res.status(200).json({load: load, truck: truck});
    } else {
      res.status(400).json({message: 'You need to be a shipper'});
    }
  }
  /**
   * @param {*} dimensions
   * @param {*} payload
   * @return {*} returns Array
   */
  getTruckSize(dimensions, payload) {
    const trucksList = [];
    states.TRUCK_PARAMS.forEach((truck) => {
      if (
        truck.payload >= payload &&
        truck.dimensions.width >= dimensions.width &&
        truck.dimensions.length >= dimensions.length &&
        truck.dimensions.height >= dimensions.height
      ) {
        trucksList.push(truck.type);
      }
    });
    return trucksList;
  }
};
