const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;
const TruckSchema = new mongoose.Schema({
  created_by: {
    type: ObjectId,
    required: true,
  },
  assigned_to: ObjectId,
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  created_date: {type: Date, default: Date.now},
});

const Truck = mongoose.model('Trucks', TruckSchema);
module.exports = Truck;
