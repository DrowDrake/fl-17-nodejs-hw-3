const mongoose = require('mongoose');
const UserSchema = new mongoose.Schema({
  password: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  createdDate: {type: Date, default: Date.now},
});

const User = mongoose.model('Users', UserSchema);
module.exports = User;
