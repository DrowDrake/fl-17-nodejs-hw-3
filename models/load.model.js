const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;
const LoadSchema = new mongoose.Schema({
  created_by: {
    type: ObjectId,
    required: true,
  },
  assigned_to: ObjectId,
  status: String,
  state: String,
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: Number,
    length: Number,
    height: Number,
  },
  logs: [
    {
      message: String,
      time: {type: Date, default: Date.now},
    },
  ],
  created_date: {type: Date, default: Date.now},
});

const Load = mongoose.model('Loads', LoadSchema);
module.exports = Load;
