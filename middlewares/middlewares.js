const logsInfo = (req, res, next) => {
  console.log(req.path);
  next();
};

const errorHeandler = (error, req, res, next) => {
  if (error) {
    res.status(500).json({message: error.message});
  }
};

module.exports = {logsInfo, errorHeandler};
