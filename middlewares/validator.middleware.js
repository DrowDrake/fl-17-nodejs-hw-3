const Validators = require('../validators/validators');

module.exports = function(validator) {
  if (!Validators.hasOwnProperty(validator)) {
    throw new Error(`'${validator}' validator is not exist`);
  }

  return async function(req, res, next) {
    try {
      if (JSON.stringify(req.body) !== '{}') {
        const validated = await Validators[validator].validateAsync(req.body);
        req.body = validated;
      } else {
        const validated = await Validators[validator].validateAsync(req.query);
        req.body = validated;
      }
      next();
    } catch (err) {
      if (err.isJoi) return res.status(400).json({message: err.message});
    }
  };
};
