const jwt = require('jsonwebtoken');

exports.generateToken = (user) =>
  jwt.sign({data: user}, process.env.TOKEN_SECRET, {expiresIn: '24h'});
