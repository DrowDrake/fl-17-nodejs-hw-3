const Auth = require('../controllers/auth.controller');
const express = require('express');
const router = new express.Router();
const validator = require('../middlewares/validator.middleware');

const authController = new Auth();

router.post('/register', validator('register'), (req, res) => {
  authController.registerUser(req, res);
});

router.post('/login', validator('login'), (req, res) => {
  authController.loginUser(req, res);
});

router.post('/forgot_password', validator('forgotPassword'), (req, res) => {
  authController.forgotPassword(req, res);
});

module.exports = router;
