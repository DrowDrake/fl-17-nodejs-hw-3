const express = require('express');
const verifyToken = require('../verifyToken');
const router = new express.Router();
const UserController = require('../controllers/user.controller');
const validator = require('../middlewares/validator.middleware');

const userController = new UserController();

router.get('/', verifyToken, (req, res) => {
  userController.getById(req, res);
});

router.delete('/', verifyToken, (req, res) => {
  userController.deleteUser(req, res);
});

router.patch(
    '/password',
    verifyToken,
    validator('changePassword'),
    (req, res) => {
      userController.changePassword(req, res);
    },
);

module.exports = router;
