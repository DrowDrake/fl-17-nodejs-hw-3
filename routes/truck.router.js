const express = require('express');
const verifyToken = require('../verifyToken');
const router = new express.Router();
const TruckController = require('../controllers/truck.controller');
const validator = require('../middlewares/validator.middleware');

const truckController = new TruckController();

router.get('/', verifyToken, (req, res) => {
  truckController.getAllTrucks(req, res);
});

router.post('/', verifyToken, validator('createTruck'), (req, res) => {
  truckController.addTruck(req, res);
});

router.get('/:id', verifyToken, (req, res) => {
  truckController.getTruckById(req, res);
});

router.put('/:id', verifyToken, validator('updateTruck'), (req, res) => {
  truckController.updateUserTruck(req, res);
});

router.delete('/:id', verifyToken, (req, res) => {
  truckController.deleteTruck(req, res);
});

router.post('/:id/assign', verifyToken, (req, res) => {
  truckController.assignTruck(req, res);
});

module.exports = router;
