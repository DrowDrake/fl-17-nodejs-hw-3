const express = require('express');
const verifyToken = require('../verifyToken');
const router = new express.Router();
const LoadController = require('../controllers/load.controller');
const validator = require('../middlewares/validator.middleware');

const loadController = new LoadController();

router.get('/', verifyToken, validator('getLoads'), (req, res) => {
  loadController.getUserLoads(req, res);
});

router.post('/', verifyToken, validator('createLoad'), (req, res) => {
  loadController.addUserLoad(req, res);
});

router.get('/active', verifyToken, (req, res) => {
  loadController.getActiveLoad(req, res);
});

router.patch('/active/state', verifyToken, (req, res) => {
  loadController.iterateToNextLoad(req, res);
});

router.get('/:id', verifyToken, (req, res) => {
  loadController.getUserLoadById(req, res);
});

router.put('/:id', verifyToken, validator('updateLoad'), (req, res) => {
  loadController.updateUserLoad(req, res);
});

router.delete('/:id', verifyToken, (req, res) => {
  loadController.deleteUserLoad(req, res);
});

router.post('/:id/post', verifyToken, (req, res) => {
  loadController.addUserLoadById(req, res);
});

router.get('/:id/shipping_info', verifyToken, (req, res) => {
  loadController.getShippingInfo(req, res);
});

module.exports = router;
